from twitter_collect.twitter_connection_setup import twitter_setup
#from CandidateData.candidate_queries import get_candidate_queries, get_tweets_from_candidates_search_queries
from twitter_collect. import get_tweets_from_candidates_search_queries, get_candidate_queries
import json
import tweepy
#from pytest import *
#from twitter_collect.collect_tweets import collect
#from twitter_collect.dataframe import transform_dataframe

#def test_collect():
#    search_term = '#elenao'
#    tweets = collect(search_term)
#    data = transform_dataframe(tweets)
#    assert 'tweet_textual_content' in data.columns


def serizer_tweets(tweet):
    if isinstance(tweet, tweepy.models.Status):
        return {"text": str(tweet.text),
                "id": tweet.id,
                "retweeted": tweet.retweeted,
                "retweet_count": tweet.retweet_count,
                "favorite_count": tweet.favorite_count,
                "created_at": tweet.created_at.isoformat(),
                "followers_count": tweet.user.followers_count,
                "description": str(tweet.user.description),
                "name": tweet.user.name}
    raise TypeError(repr(tweet) + " Not the same object type")


def store_tweets(tweets,filename):
    json_to_dump = []
    for query in tweets:
        print(query.__class__)
        for tweet in query:
            json_to_dump.append(serizer_tweets(tweet))
    with open(filename, 'w', encoding='utf-8') as f:
        json.dump(json_to_dump, f, indent=4)


if __name__ == "__main__":  # If we execute this file
    num_condidat = 1
    queries = get_candidate_queries(num_condidat, 'C:\\Users\\samuk\\PycharmProjects\\twitterPredictor\\CandidateData\\')
    api_instance = twitter_setup()
    queries_results = get_tweets_from_candidates_search_queries(queries, api_instance)
    output_filename = '../jsonDump/tweets_condidat_'+ str(num_condidat) + '.json'
    store_tweets(queries_results, output_filename)
