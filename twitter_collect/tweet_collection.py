from twitter_collect.twitter_connection_setup import twitter_setup
from twitter_collect.collect_candidate_actuality_tweets import get_candidate_queries, get_tweets_from_candidates_search_queries
import json
import tweepy

def serizer_tweets(tweet):
    if isinstance(tweet, tweepy.models.Status):
        return {"text": str(tweet.text),
                "id": tweet.id,
                "retweeted": tweet.retweeted,
                "retweet_count": tweet.retweet_count,
                "favorite_count": tweet.favorite_count,
                "created_at": tweet.created_at.isoformat(),
                "followers_count": tweet.user.followers_count,
                "description": str(tweet.user.description),
                "name": tweet.user.name}
    raise TypeError(repr(tweet) + " Not the same object type")

def store_tweets(list_list_status, filepath):
    json_to_dump = []

    for list_status in list_list_status:
        print(list_status.__class__)

        for tweet_status in list_status:
            json_to_dump.append(serizer_tweets(tweet_status))

    with open(filepath, 'w', encoding='utf-8') as f:
        json.dump(json_to_dump, f, indent=4)

if __name__ == "__main__":  # If we execute this file
    num_condidat = 0
    queries = get_candidate_queries(num_condidat, '../CandidateData')
    API = twitter_setup()
    list_list_status = get_tweets_from_candidates_search_queries(queries, API)
    output_filepath = '../jsonDump/tweets_condidat_' + str(num_condidat) + '.json'
    store_tweets(list_list_status, output_filepath)
