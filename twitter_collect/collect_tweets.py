#Libraries and modules imports
from twitter_collect import twitter_connection_setup #Import of twitter's API authentified

def collect(phrase,language,number):
    """
    This function collects the Status returned by
    the phrase in function of language that we're looking for
    :param phrase: phrase we're looking for
    :param language: language that we want to search
    :param number: number of tweets we want to
    :return: Number defined of Status of the tweets
    """
    API = twitter_connection_setup.twitter_setup() #API authentified
    tweets_of_status = API.search(phrase,language=language,rpp=number)

    return tweets_of_status #Return a list of twets status


def collect_texts(phrase,language,number):
    """
    This function collects the Texts returned by
    the phrase in function of language that we're looking for
    :param phrase: phrase we're looking for
    :param language: language that we want to search
    :param number: number of tweets we want to
    :return: Number defined of Texts of the tweets
    """
    tweets_text = []

    #Collects the text in each Status
    for tweet in collect(phrase,language,number):
       tweets_text.append(tweet.text) #list with the texts of tweets

    return tweets_text #Return a list of twitters text


#Test of the function
if __name__ == "__main__":  # If we execute this file
    print(collect("CentraleSupélec", 'french', 10)) #Print our Status collected
    print(collect_texts("CentraleSupélec", 'french', 10)) #Print our Texts collected




