#Libraries and modules imports
from twitter_collect import twitter_connection_setup #Import of twitter's API authentified

def collect_by_user(user_id,number):
    """
    This function collects the Status returned by
    the user that we're looking for
    :param user_id: name of the twitter or its id number
    :param number: number of tweets we want to
    :return: Number defined of user's tweets Status
    """

    API = twitter_connection_setup.twitter_setup() #API authentified
    users_tweets_status = API.user_timeline(id = user_id, count = number)

    return users_tweets_status #Return of users tweets status

def collect_texts_by_user(user_id,number): #the user_id can be the name of the twitter or an id number
    """
    This function collects the Texts returned by
    the user that we're looking for
    :param user_id: name of the twitter or its id number
    :param number: number of tweets we want to
    :return: Number defined of user's tweets Texts
    """
    tweets_text = []

    #Collects the text in each Status
    for tweet in collect_by_user(user_id,number):
       tweets_text.append(tweet.text) #list with the texts of tweets

    return tweets_text #Return a list of twitters text


#Test of the function
if __name__ == "__main__":  # If we execute this file
    print(collect_by_user("EmmanuelMacron", 10)) #Print our Status collected
    print(collect_texts_by_user("EmmanuelMacron", 10)) #Print our Texts collected
