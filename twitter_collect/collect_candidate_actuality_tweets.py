#Libraries and modules imports
import os
from tweepy.error import RateLimitError
from twitter_collect.collect_tweets import collect

def get_candidate_queries(num_candidate, file_path):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag
    files
    :return: a list of string queries that can be done to the search API independently
    """
    file_name_keywords = 'keywords_candidate_' + str(num_candidate) + '.txt'
    file_name_hashtags = 'hashtag_candidate_' + str(num_candidate) + '.txt'

    path_keywords = os.path.join(file_path, file_name_keywords)
    path_hashtags = os.path.join(file_path, file_name_hashtags)

    try:
        #Open the files and read its lines
        keywords = open(path_keywords).readlines()
        hashtags = open(path_hashtags).readlines()

        #Print the keywords and hashtags founded in the files
        print('The loaded keywords : {}'.format(keywords))
        print('The loaded hashtags : {}'.format(hashtags))

        return keywords+hashtags #Return a list with the words

    except IOError:
        #If there is an error
        print('The file cannot be opened, please check the given path / condidate number')


def get_tweets_from_candidates_search_queries(queries, language, number):
    '''
    Now we'll do a term based search for each keyword and hashtag and return the queries
    :param queries: queries with the keywords and hashtagas
    :param language: language that we want to search
    :param number: number of tweets we want to
    :return: a list of Status
    '''
    results = []
    try:

        for query in queries: #Search the tweets for each word in the queries
            query_result = collect(query,language,number)
            results.append(query_result)

    except RateLimitError:
        #If there is an error
        print('An error occurred during the query, please check the API')

    return results #Return a list of list of status

############## Testing #################
if __name__ == "__main__": # If we execute this file
    queries = get_candidate_queries(0, '../CandidateData') #Search for Macron (Candidate 0)
    query_results = get_tweets_from_candidates_search_queries(queries, "french", 5)

    for tweet in query_results[4]: #Print the tweets related to the nth word
        print('-> {}\n'.format(tweet.text))


