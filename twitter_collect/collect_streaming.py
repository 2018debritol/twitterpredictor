#Libraries and modules imports
import tweepy
from twitter_collect import twitter_connection_setup #Import of twitter's API authentified

class StdOutListener(tweepy.StreamListener):
    """
    Creation of a class to check the acess to conect streaming API
    :return: True = no problem, False = exceed numbers of attempts
    """
    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        if  str(status) == "420":
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True




def collect_by_streaming(tracked_phrase):
    """
    This function collects the Status returned by
    the tracked phrase that we're looking for
    :param tracked_phrase: phrase we're looking for
    :return: Show in streaming the twitters with the tracked phrase
    """

    API = twitter_connection_setup.twitter_setup() #API authentified

    listener = StdOutListener()
    stream =  tweepy.Stream(auth = API.auth, listener=listener)
    stream.filter(track=[tracked_phrase], async=True)

#Test of the function
if __name__ == "__main__":  # If we execute this file
    collect_by_streaming("France") #Status with the tracked phrase
