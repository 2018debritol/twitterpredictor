import multidict as multidict
import numpy as np
import os
import re
from PIL import Image
from os import path
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from twitter_collect.collect_tweets import collect


def getFrequencyDictForText(sentence):
    fullTermsDict = multidict.MultiDict()
    tmpDict = {}

    # making dict for counting frequencies
    for text in sentence.split(" "):
        if re.match("a|the|an|the|to|in|for|of|or|by|with|is|on|that|be", text):
            continue

        val = tmpDict.get(text, 0)
        tmpDict[text.lower()] = val + 1
    for key in tmpDict:
        fullTermsDict.add(key, tmpDict[key])
    return fullTermsDict


def makeImage(text):
    Simpson_mask = np.array(Image.open("../birdtwitter.jpg"))

    wc = WordCloud(background_color="white", max_words=1500, mask=Simpson_mask)
    # generate word cloud
    wc.generate_from_frequencies(text)

    # show
    plt.imshow(wc, interpolation="bilinear")
    plt.axis("off")
    plt.show()


# get data directory (using getcwd() is needed to support running example in generated IPython notebook)
d = path.dirname(__file__) if "__file__" in locals() else os.getcwd()

tweets_text = []
text = ''

#Take the text of tweets list
list_tweets_status = collect('Obama','english',5000)

for tweets_status in list_tweets_status:
    tweets_text.append(tweets_status.text)

#Transform the all text into TextBlob
for text_temp in tweets_text:
    text += text_temp

makeImage(getFrequencyDictForText(text))
