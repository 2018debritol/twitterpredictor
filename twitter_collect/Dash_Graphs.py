import dash
import dash_core_components as dcc
import dash_html_components as html
from twitter_collect.Opinion_Analysis import sentiment_analysis

def Generate_Axes(twitter_candidate, number_of_tweets):

    x_axe= number_of_tweets
    y_positive_axe = []
    y_neutral_axe = []
    y_negative_axe = []
    y_axe = []

    for number in x_axe:
        result_temp = sentiment_analysis(twitter_candidate, number)
        total = result_temp[0] + result_temp[1] + result_temp[2]

        y_positive_axe.append((result_temp[0]/total)*100)
        y_neutral_axe.append((result_temp[1]/total)*100)
        y_negative_axe.append((result_temp[2]/total)*100)

    y_axe.append(y_positive_axe)
    y_axe.append(y_neutral_axe)
    y_axe.append(y_negative_axe)

    return x_axe,y_axe




def Dash_Bar_Graph(twitter_candidate,number_of_tweets):

    x_axe = []
    y_axe = []

    x_axe,y_axe = Generate_Axes(twitter_candidate,number_of_tweets)

    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

    Dash_Graphs = dash.Dash(__name__, external_stylesheets=external_stylesheets)

    Dash_Graphs.layout = html.Div(children=[
        html.H1(children='CentraleSupélec'),

        html.Div(children='''
            Twitter Election Prediction
        '''),

        dcc.Graph(
            id='example-graph',
            figure={
                'data': [
                    {'x': x_axe, 'y': y_axe[0], 'type': 'bar', 'name': 'Positive Twitters'},
                    {'x': x_axe, 'y': y_axe[1], 'type': 'bar', 'name': 'Neutral Twitters'},
                    {'x': x_axe, 'y': y_axe[2], 'type': 'bar', 'name':  'Negative Twitters'},
                ],
                'layout': {
                    'title': 'Sentimental Analysis for different quantities of tweets'
                }
            }
        )
    ])

    Dash_Graphs.run_server(debug=True)


Dash_Bar_Graph("neymarjr",[50,100,150,200])
