#Libraries and modules imports
from twitter_collect.collect_tweets_users import collect_by_user

def get_retweets_of_candidate(candidate_username, number):
    """
    This function collects the Status returned by
    the user that we're looking for
    :param candidate_username: name of the twitter or its id number
    :param number: number of retweets we want to
    :return: Number of retweets per tweet
    """

    status = collect_by_user(candidate_username, number)
    retweets_per_tweet = {}

    for tweet in status:
        # Only for original tweets, excluding the retweets
        if (not tweet.retweeted) or ('RT @' not in tweet.text):
            retweets_per_tweet[tweet.text] = tweet.retweet_count
            
    return retweets_per_tweet #Return a dictionary with the number of retweets

############## Testing #################
if __name__ == "__main__": # If we execute this file
    username = 'EmmanuelMacron'
    number = 5
    retweets_per_tweet = get_retweets_of_candidate(username,number)

    for tweet, nb_rt in retweets_per_tweet.items():
        print('{}, the number of retweets are --> {}\n'.format(tweet, nb_rt))
